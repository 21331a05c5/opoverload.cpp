#include <iostream>
using namespace std;
class OpOverload{
    private:
        int a, b;
    public:
        OpOverload(int r=0, int i=0){
            a=r;
            b=i;
        }
        OpOverload operator+(OpOverload const &obj){
            OpOverload result;
            result.a= a+ obj.a;
            result.b= b+ obj.b;
            return result;
        }
        void displayMsg(){
            cout<<"a = "<<a<<endl;
            cout<<"b = "<<b<<endl;
        }
};
int main() {
    OpOverload o1(1,2);
    OpOverload o2(3,4);
    OpOverload o3 = o1 + o2;
    o3.displayMsg();
    return 0;
}
